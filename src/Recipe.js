import React, { useState } from "react";
import { scryRenderedDOMComponentsWithClass } from "react-dom/test-utils";

const Recipe = (props) => {
  const { title, calories, image, waystomake, dietreco } = props;
  const [ingredients, setingredients] = useState(false);
  const [calorieinfo, setcalorieinfo] = useState(false);
  const [dietlabel, setdietlabel] = useState(false);

  const seting = (event) => {
    setingredients(!ingredients);
    setcalorieinfo(false);
    setdietlabel(false);
  };
  const setcal = (event) => {
    setcalorieinfo(!calorieinfo);
    setingredients(false);
    setdietlabel(false);
  };
  const setd = (event) => {
    setdietlabel(!dietlabel);
    setingredients(false);
    setcalorieinfo(false);
  };
  let p1 = null;
  if (ingredients) {
    p1 = (
      <div>
        The ways to make {title} are as follows.
        <ol>
          {waystomake.map((way) => {
            return <li>{way}</li>;
          })}
        </ol>
      </div>
    );
  }
  let p2 = null;
  if (calorieinfo) {
    p2 = (
      <div>
        Calories in {title} are {calories}.
      </div>
    );
  }
  let p3 = null;
  if (dietlabel) {
    p3 = (
      <div>
        This food is -{">"}
        <ul>
          {dietreco.map((sing) => {
            return <li>{sing}</li>;
          })}
        </ul>
      </div>
    );
  }
  return (
    <div className="recipe">
      <h1> Item - {title}</h1>
      <img src={image} />
      <div>
        <button onClick={(event) => seting(event)}>ingredients</button>
        <button onClick={(event) => setcal(event)}>Calorie Info.</button>
        <button
          onClick={(event) => {
            setd(event);
          }}
        >
          DietLabel
        </button>
        {p1}
        {p2}
        {p3}
      </div>
    </div>
  );
};

export default Recipe;
