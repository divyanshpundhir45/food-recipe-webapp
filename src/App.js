import React, { useEffect, useState } from "react";
import "./App.css";
import axios from "axios";
import Recipe from "./Recipe";

const App = () => {
  const APP_ID = "fa4e5369";
  const APP_KEY = "9eb871653b825d129fbf3033e5e5e8f9";

  const [recipes, setRecipes] = useState([]);
  const [search, setSearch] = useState("");
  const [valuetosearch, setValuetoSearch] = useState("strawberry");
  useEffect(() => {
    getRecipes();
    console.log("we are fetching  data");
  }, [valuetosearch]);
  const searchfunction = (event) => {
    setSearch(event.target.value);
  };
  const getRecipes = () => {
    axios
      .get(
        `https://api.edamam.com/search?q=${valuetosearch}&app_id=${APP_ID}&app_key=${APP_KEY}`
      )
      .then((response) => {
        console.log(response.data.hits);
        setRecipes(response.data.hits);
      });
  };
  const setQuery = (event) => {
    event.preventDefault();
    setValuetoSearch(search);
  };
  return (
    <div className="App">
      <form className="search-form" onSubmit={(event) => setQuery(event)}>
        <input
          className="search-bar"
          type="text"
          value={search}
          onChange={(event) => searchfunction(event)}
        />
        <button className="search-button" type="submit">
          Search
        </button>
      </form>
      <div className="recipes">
        {recipes.map((rec, index) => {
          return (
            <Recipe
              key={index}
              title={rec.recipe.label}
              calories={rec.recipe.calories}
              image={rec.recipe.image}
              waystomake={rec.recipe.ingredientLines}
              dietreco={rec.recipe.healthLabels}
            />
          );
        })}
      </div>
    </div>
  );
};

export default App;
